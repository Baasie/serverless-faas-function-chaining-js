import {Callback, Context} from "aws-lambda";
import {LicensePlate} from "./entities/license-plate";
import {Response} from './support/response';

export function handle(event: any, context: Context, callback: Callback) {

    let licensePlate: LicensePlate;
    try {
        licensePlate = LicensePlate.fromJSON(JSON.parse(event.body as string));
    } catch (error) {
        return callback(null, Response.BAD_REQUEST(error));
    }

    return callback(null, Response.OK(licensePlate.number));
}