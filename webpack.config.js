// Import path for resolving file paths
var path = require('path');
var entry = require('webpack-glob-entry')

module.exports = {
    target: 'node', // in order to ignore built-in modules like path, fs, etc.
    mode: 'production',
    externals: {
        'aws-sdk': 'aws-sdk'
    },

    // Specify the entry point for our lambda's.
    // entry: entry(entry.basePath('src'), 'src/**/*.lambda.ts'),
    entry: {
        'request-car-entry.lambda': './src/infra/api/request-car-entry.lambda.ts',
        'confirm-appointment.lambda': './src/application/confirm-appointment.lambda.ts',
        'confirm-employee.lambda': './src/application/confirm-employee.lambda.ts'
    },

    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.jsx']
    },

    // Specify the output file containing our bundled code
    output: {
        path: path.join(__dirname, "./dist/"),
        library: "[name]",
        libraryTarget: "commonjs2",
        filename: "[name].js"
    },
    module: {
        rules: [
            {
                test: /\.json$/,
                exclude: /node_modules/,
                loaders: ['json']
            },
            {
                test: /\.tsx?$/,
                exclude: /tests/,
                loader: 'ts-loader',
                options: {
                    configFile: 'tsconfig.json'
                }
            }
        ]
    }
};